import os
import sys
import requests
import json
import tweepy
import logging
from logging.handlers import RotatingFileHandler
import time
import random

class basicTwitterApp():
    noStatusUpdate = False
    debugMode = False
    consoleLogging = False
    maxLogFileSize = 1024 * 4000 # 4MB
    maxTweetCharacters = 280
    credentials=[]
    currentToken = 0
    updateToken = 0
    api = None
    maxFollowLookups = 0
    maxFollowLookupsPerUser = 2
    currentFollowsLookupCount = 0 # needs to be reset in APP
    maxSearchResults = 100
    sleepTimeRateLimit = 900 # seconds
    # Example WOEID locations include: Worldwide: 1 UK: 23424975 Brazil: 23424768 Germany: 23424829 
    # Mexico: 23424900 Canada: 23424775 United States: 23424977 New York: 2459115
    woeid=23424829

    # ─── SETUP ──────────────────────────────────────────────────────────────────────

    def setupLogging(self):
        log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
        if self.consoleLogging:
            my_handler = logging.StreamHandler()
        else:
            logFile = os.path.join(os.path.dirname(__file__), "log")
            my_handler = RotatingFileHandler(logFile, mode='a', maxBytes=self.maxLogFileSize, 
                                        backupCount=2, encoding=None, delay=0)
        my_handler.setFormatter(log_formatter)
        app_log = logging.getLogger()
        if self.debugMode:
            app_log.setLevel(logging.DEBUG)
        else:
            app_log.setLevel(logging.INFO)
        app_log.addHandler(my_handler)
        logging.debug("log level set to DEBUG")

    def readCredentials(self, path):
        logging.debug(path)
        if os.path.exists(path):
            f = open(path,)
            self.credentials = json.load(f)
        else:
            logging.error("no credentials found, exit!")
            exit()

    def setUpAPI(self):
        if self.credentials:
            updateCredentials = self.credentials[self.updateToken]
            # authorization of consumer key and consumer secret
            auth = tweepy.OAuthHandler(updateCredentials["apikey"], updateCredentials["apikey_secret"])
            # set access to user's access key and access secret 
            auth.set_access_token(updateCredentials["acess_token"], updateCredentials["acess_token_secret"])
            # calling the api 
            self.api = tweepy.API(auth)
        else:
            logging.error("no credentials found, exit!")
            exit()

    # ─── API CALLS ──────────────────────────────────────────────────────────────────

    def getTrends(self):
        trends = self.api.get_place_trends(id = self.woeid)
        trend_list = []
        for value in trends:
            for trend in value['trends']:
                trend_list.append(trend['name'])
        trendListString = ""
        for trend in trend_list: 
            trendListString = f"{trendListString}\n{trend}"
        logging.info(f"\nThe trends for the location ({self.woeid}) are:\n{trendListString}")
        return trend_list

    def updateStatus(self, tweets, replyID=None):
        replyTo = replyID
        for tweet in tweets:
            if self.noStatusUpdate:
                logging.warning(f'status update disabled (debug dummy):\n"{tweet}"\n')
            else:
                try:
                    if replyTo:
                        status = self.api.update_status(tweet, card_uri='tombstone://card', in_reply_to_status_id=str(replyTo), auto_populate_reply_metadata=True)
                        msg = f'status update {status.id_str}, reply to {replyTo}:\n'
                    else:
                        status = self.api.update_status(tweet, card_uri='tombstone://card') # card_uri='tombstone://card' prefents link preview
                        msg = f'status update {status.id_str}:\n\n'
                    logging.info(f'"{msg}{tweet}"\ntweet length: {len(tweet)}')
                    replyTo = status.id_str
                except Exception as e:
                    logging.warning("failed to update status! Exception: {0}".format(e))
                    replyTo = None
                    break
        return replyTo

    def countRetweetsAndLikes(self, data):
        retweets_count = 0
        like_count = 0
        reply_count = 0
        for tweet in data:
            like_count += tweet["public_metrics"]["like_count"]
            reply_count += tweet["public_metrics"]["reply_count"]
            if tweet["text"].startswith('RT @', 0, 4):
                retweets_count += 1
        return retweets_count, like_count, reply_count

    def searchCountRequest(self, hashtag):
        hashtag = hashtag.replace("'", "") 
        search_url = "https://api.twitter.com/2/tweets/counts/recent"
        query_params = {'query': hashtag}
        result = self.connect_to_endpoint(search_url, query_params)
        if result and "meta" in result:
            count = result["meta"]["total_tweet_count"]
            return count
        else:
            return None

    def searchRequest(self, hashtag, randomOrder=True):
        hashtag = hashtag.replace("'", "") 
        search_url = "https://api.twitter.com/2/tweets/search/recent"
        query_params = {'query': hashtag,'tweet.fields': 'author_id,public_metrics', 'max_results': 100}
        data = []
        next_token = None
        # get multiple search result pages. If next_token exist, there are more results
        maxSearchPages = int(self.maxSearchResults/100)
        logging.info(f"keyword: {hashtag}, maxSearchResults: {self.maxSearchResults}, maxSearchPages {maxSearchPages}")
        logging.debug(f"query_params: {query_params}")
        for i in range(maxSearchPages):
            if next_token:
                query_params["next_token"] = next_token
            result = self.connect_to_endpoint(search_url, query_params)
            if result and "data" in result:
                data.extend(result["data"])
                if "next_token" in result["meta"]:
                    next_token = result["meta"]["next_token"]
                    logging.debug(f"found next token {next_token}")
                else:
                    break
            else:
                break
        
        tweetCount = len(data)
        retweets, likes, replys = self.countRetweetsAndLikes(data)
        logging.info(f'search result: found {len(data)} tweets about "{hashtag}", retweets: {retweets}')
        # randomize order
        if randomOrder:
            random.shuffle(data)
        return data, tweetCount, retweets, likes, replys

    def bearer_oauth(self, r):
        """
        Method required by bearer token authentication.
        """
        r.headers["Authorization"] = "Bearer {0}".format(self.credentials[self.currentToken]["bearer_token"])
        r.headers["User-Agent"] = "v2RecentSearchPython"
        return r

    def getNextBearerToken(self):
        self.currentToken = self.currentToken + 1
        if self.currentToken >= len(self.credentials):
            self.currentToken = 0
        logging.info(f"getting next token: {self.currentToken}")

    def connect_to_endpoint(self, url, params, skipOnBadRespons=False):
        responceCode=0
        try:
            while responceCode != 200:
                response = requests.get(url, auth=self.bearer_oauth, params=params)
                responceCode = response.status_code
                logging.debug(responceCode)
                if responceCode != 200:
                    logging.warning(f'bad response {responceCode}, "{response.text}"')
                    if skipOnBadRespons:
                        logging.warning("skip request because of bad response")
                        return None
                    elif responceCode == 429 or responceCode == 410:
                        if self.currentToken == len(self.credentials) -1:
                            logging.warning(f"token cooldown, sleep for {self.sleepTimeRateLimit} seconds")
                            time.sleep(self.sleepTimeRateLimit)
                        self.getNextBearerToken()
                    else:
                        return response.json()
                else:
                    return response.json()
        except Exception as e:
            logging.warning("failed to connect to endpoint! Exception: {0}".format(e))
            return None

    def getFollowersRequest(self, user_id):
        url = "https://api.twitter.com/2/users/{}/followers".format(user_id)
        params = {"user.fields": "created_at", "max_results": 1000}
        logging.info(f"get Follower: {user_id}")
        result = self.connect_to_endpoint(url, params)
        if result and "data" in result:
            logging.debug(f"followers: {result}")
            return [user["id"] for user in result["data"]]
        else:
            logging.warning(f"no followers found for: {user_id}")
            return []

    def getBlockingRequest(self, user_id):
        url = "https://api.twitter.com/2/users/{}/blocking".format(user_id)
        params = {"user.fields": "created_at", "max_results": 1000}
        logging.info(f"get blocking: {user_id}")
        result = self.connect_to_endpoint(url, params)
        if result and "data" in result:
            logging.debug(f"blocking: {result}")
            return [user["id"] for user in result["data"]]
        else:
            logging.warning(f"no blocking found for: {user_id}")
            return []

    def getUserRequest(self, user_id):
        url="https://api.twitter.com/2/users/{}".format(user_id)
        params = {"user.fields": "public_metrics"}
        logging.info(f"get user: {user_id}")
        result = self.connect_to_endpoint(url, params)
        if result and "data" in result:
            logging.debug(f"user info: {result}")
            return result["data"]
        else:
            logging.warning(f"user not found: {user_id}")
            return None

    def getFollowingRequest(self, user_id):
        url = "https://api.twitter.com/2/users/{}/following".format(user_id)
        params = {"user.fields": "created_at", "max_results": 1000}
        logging.info(f"get Following: {user_id}")
        next_token = None
        following = []
        for i in range(self.maxFollowLookupsPerUser):
            if next_token:
                params["pagination_token"] = next_token
            result = self.connect_to_endpoint(url, params)
            self.currentFollowsLookupCount += 1
            if result and "data" in result:
                following.extend([user["id"] for user in result["data"]])
                if "next_token" in result["meta"]:
                    next_token = result["meta"]["next_token"]
                    logging.debug(f"found next token {next_token}")
                else:
                    break
            else:
                logging.warning(f"no following found for: {user_id}")
                break
        logging.debug(f"following of {user_id}: {len(following)} (count)")
        return following

    def getTweets(self, tweet_ids):
        logging.info(f"get Tweets: {tweet_ids}")
        tweets = ""
        for i, tweet in enumerate(tweet_ids):
            tweets += str(tweet)
            if i + 1 < len(tweet_ids):
                tweets += ","
        params = {"ids": tweets, "tweet.fields": "lang,author_id,public_metrics,referenced_tweets"}
        # Tweet fields are adjustable.
        # Options include:
        # attachments, author_id, context_annotations,
        # conversation_id, created_at, entities, geo, id,
        # in_reply_to_user_id, lang, non_public_metrics, organic_metrics,
        # possibly_sensitive, promoted_metrics, public_metrics, referenced_tweets,
        # source, text, and withheld
        # You can adjust ids to include a single Tweets.
        # Or you can add to up to 100 comma-separated IDs
        # url = "https://api.twitter.com/2/tweets?{}&{}".format(tweets, tweet_fields)
        url = "https://api.twitter.com/2/tweets?"
        result = self.connect_to_endpoint(url, params)
        return reslut

    def getLikingUsers(self, id, randomOrder=True):
        logging.info(f"get liking users of: {str(id)}")
        url = "https://api.twitter.com/2/tweets/{}/liking_users".format(id)
        params = {"tweet.fields": "lang,author_id", "max_results": 100}
        liking_user = []
        next_token = None
        for i in range(75):
            if next_token:
                params["pagination_token"] = next_token
            result = self.connect_to_endpoint(url, params)
            if result and "data" in result:
                liking_user.extend([user["id"] for user in result["data"]])
                if "next_token" in result["meta"]:
                    next_token = result["meta"]["next_token"]
                else:
                    break
            else:
                break
        logging.info(f"found {len(liking_user)} of tweet {str(id)}")
        if randomOrder:
            random.shuffle(liking_user)
        return liking_user


    # ─── UTILS ──────────────────────────────────────────────────────────────────────

    def getTweetStringFromLines(self, lines, firstLine, lastLine, prev="", pageNumbers=None):
        tweetString = prev
        lineCount = 0
        num = ""
        if pageNumbers:
            num = f"\n\n{pageNumbers[0]}/{pageNumbers[1]}"
        
        for i in range(max(firstLine, 0), min(len(lines), lastLine)):
            line = lines[i]
            if len(tweetString) + len(line) > self.maxTweetCharacters - len(num):
                break
            tweetString = tweetString + line
            if i < lastLine:
                tweetString += "\n"
            lineCount += 1
        if pageNumbers:
            tweetString += num
        return tweetString, lineCount