import os
import sys
import collections
import shutil
import time
import json
from datetime import datetime, timedelta
import argparse
import logging
import TwitterApp

class statistikBot(TwitterApp.basicTwitterApp):
    trendRange = 8
    rankingRange = 10
    trendIntervallTime = 14 # hours
    sleepTimeSkip = 60
    Cache = {}
    cacheFileName = "Cache"
    cacheFolder = None
    cacheFile = None
    cacheTimeRange = 7 # days 
    onlyUseCache = False

    # ─── CACHE ──────────────────────────────────────────────────────────────────────

    def getFormatedTime(self, timeString):
        startDate = datetime.fromisoformat(timeString)
        return f"_{startDate.year}_{startDate.month}_{startDate.day}"

    def getCacheTemplate(self):
        return {"startDate": datetime.now().isoformat(), "trends": {}, "users": {}, "rankedUsers": {}}

    def cacheIntervallFinished(self):
        startTime = datetime.fromisoformat(self.Cache["startDate"])
        return startTime < datetime.now() - timedelta(days=self.cacheTimeRange)

    def saveCache(self):
        def saveCacheFile():
            if not os.path.exists(self.cacheFolder):
                os.makedirs(self.cacheFolder)
            logging.info(f"start saving Cache")
            try:
                with open(self.cacheFile, 'w') as f:
                    json.dump(self.Cache, f)
                logging.info(f"save Cache: {self.cacheFile}")
            except Exception as e:
                logging.warning(f"failed to save Cache: {self.cacheFile}! Exception: {e}")

        if self.cacheIntervallFinished():
            self.Cache["endDate"] = datetime.now().isoformat()
            saveCacheFile()
            cacheBackupFile = os.path.join(self.cacheFolder, self.cacheFileName + self.getFormatedTime(self.Cache["startDate"])+".json")
            shutil.copy(self.cacheFile, cacheBackupFile)
            self.Cache = self.getCacheTemplate()
            logging.info(f'cache interval finished. Backup cache file "{cacheBackupFile}" and start new interval.')
        else:
            saveCacheFile()

    def loadCache(self):
        self.cacheFolder = os.path.join(os.path.dirname(__file__), "Cache") 
        self.cacheFile = os.path.join(self.cacheFolder, self.cacheFileName + ".json")
        if os.path.exists(self.cacheFile):
            f = open(self.cacheFile)
            self.Cache = json.load(f)
            logging.debug(f"load Cache: {self.cacheFile}")
            return True
        else:
            logging.warning(f"missing Cache: {self.cacheFile}")
            self.Cache = self.getCacheTemplate()
            return None

    def addTrendToCache(self, trend, users):
        if trend in self.Cache["trends"]:
            trendSet = set(self.Cache["trends"][trend]["autoren"])
            trendSet.union(set(users))
            self.Cache["trends"][trend]["autoren"] = list(trendSet)
        else:
            self.Cache["trends"][trend] = {"autoren": users}
        self.Cache["trends"][trend]["lastdate"] = datetime.now().isoformat()

    def addUserToCache(self, user_id, user):
        self.Cache["users"][user_id] = user

    def addRankedUserToCache(self, user_id, user):
        self.Cache["rankedUsers"][user_id] = user


    # ─── STATISTICS ─────────────────────────────────────────────────────────────────

    def checkTrendTime(self, trend):
        if trend in self.Cache["trends"]:
            if "lastdate" in self.Cache["trends"][trend]:
                lastdate = datetime.fromisoformat(self.Cache["trends"][trend]["lastdate"])
                return lastdate < datetime.now() - timedelta(hours=self.trendIntervallTime)
        return True

    def getRelatedUsers(self, tweets):
        userSet = set()
        for tweet in tweets:
            userSet.add(tweet["author_id"])
        autor_count = len(userSet)
        tweetRate = str(round(len(tweets)/autor_count, 1)).replace('.', ',')
        userList = list(userSet)
        return userList, tweetRate, autor_count

    def getCommonLeader(self, users):
        followingLists = []
        self.currentFollowsLookupCount = 0
        count = 0
        countCached = 0
        logging.info(f"get common leader for {len(users)} autors")
        for user_id in users:
            if user_id in self.Cache["users"]:
                logging.info(f"load cached user: {user_id}")
                follows = self.Cache["users"][user_id]["follows"]
                followingLists.extend(follows)
                count += 1
                countCached += 1
            elif self.currentFollowsLookupCount < self.maxFollowLookups:
                if not self.onlyUseCache:
                    user = self.getUserRequest(user_id)
                    if user and 'public_metrics' in user:
                        followsCount = user["public_metrics"]["following_count"]
                        if followsCount <= 1000 * self.maxFollowLookupsPerUser: # ignore user with to many follows
                            follows = self.getFollowingRequest(user_id)
                            user["follows"] = follows
                            self.addUserToCache(user_id, user)
                            followingLists.extend(follows)
                            count += 1
                        else:
                            logging.info(f"skip user {user_id}. to many follows {followsCount}")
            else:
                break

        counter = collections.Counter(followingLists)
        logging.info(f"user count: {len(users)}, analyzed: {count}, lookups: {self.currentFollowsLookupCount}, cached: {countCached}")
        rangeCount = 1
        resultLines = []
        for user_id, amount in counter.most_common():
            user = self.getUserRequest(user_id)
            if not user:
                continue
            self.addRankedUserToCache(user_id, user)
            percent = str(round(amount / count * 100, 1)).replace('.', ',')
            name = user["name"]
            username = user["username"]
            logging.info(f'{amount} Nutzer folgen "{name}')
            if rangeCount <= self.rankingRange:
                line = f"{percent} % {name} ({username})"
                resultLines.append(line)
            else:
                break
            rangeCount += 1
        return resultLines

    def tweetTrendStats(self, trend, reply_to=None):
        logging.info(f"get trends statistics: {trend}")
        trendStartTime = time.time()
        trend_result, tweetCount, retweetCount, likesCount, replyCount = self.searchRequest(trend)
        tweetCountAll = self.searchCountRequest(trend)
        if trend_result and tweetCountAll:
            trendUsers, tweetRate, userCount = self.getRelatedUsers(trend_result)
            self.addTrendToCache(trend, trendUsers)
            tweetString = f'"{trend}" Twitternde folgen zu\n'
            rankingList = self.getCommonLeader(trendUsers)
            likeRate = str(round(likesCount / tweetCount, 1)).replace('.', ',')
            if len(rankingList) < self.rankingRange:
                logging.warning(f"invalid ranking results. Skip statistic")
                return
            else:
                tweetString, lineCount = self.getTweetStringFromLines(rankingList, 0, 5, prev=tweetString, pageNumbers=(1, 3))
                tweets = [tweetString]
                tweetString, lineCount = self.getTweetStringFromLines(rankingList, lineCount, self.rankingRange, pageNumbers=(2, 3))
                tweets.append(tweetString)
                additionalStatsLines = [
                        f"Tweets insgesamt: {tweetCountAll}",
                        f"analysierte Tweets (letzte): {tweetCount}",
                        f"einfache Tweets: {tweetCount - retweetCount}",
                        f"Retweets: {retweetCount}",
                        f"Autoren: {userCount}",
                        f"⌀ {tweetRate} Tweets pro Autor",
                        f"⌀ {likeRate} Likes pro Tweet"]
                
                additionalStats, lineCount = self.getTweetStringFromLines(additionalStatsLines, 0, 10, pageNumbers=(3, 3))
                tweets.append(additionalStats)
                self.updateStatus(tweets, replyID=reply_to)
                trendDuration =   round(time.time() - trendStartTime, 2)
                logging.info(f"time to get trend: {trendDuration}")
                if not self.onlyUseCache:
                    self.saveCache()
        else:
            logging.info(f'invalid trend: "{trend}"". Sleep for {self.sleepTimeSkip} seconds')
            time.sleep(self.sleepTimeSkip)
            self.getNextBearerToken()

    def tweetLikeStats(self, tweet_id, reply_to=None):
        likingUsers = self.getLikingUsers(tweet_id)
        logging.info(f"get like statistics: {str(tweet_id)}")
        tweetString = f'Tweet {str(tweet_id)} Likende folgen zu\n'
        rankingList = self.getCommonLeader(likingUsers)
        if len(rankingList) < self.rankingRange:
            logging.warning(f"invalid ranking results. Skip statistic")
            return
        else:
            tweetString, lineCount = self.getTweetStringFromLines(rankingList, 0, 5, prev=tweetString, pageNumbers=(1, 2))
            tweets = [tweetString]
            tweetString, lineCount = self.getTweetStringFromLines(rankingList, lineCount, self.rankingRange, pageNumbers=(2, 2))
            tweets.append(tweetString)
            self.updateStatus(tweets, replyID=reply_to)
            if not self.onlyUseCache:
                self.saveCache()

    def trendLoop(self):
        while True:
            intervalStartTime = time.time()
            trends = self.getTrends()
            for i in range(self.trendRange):
                toptrend = trends[i]
                if self.checkTrendTime(toptrend):
                   self.tweetTrendStats(toptrend)
                else:
                    logging.info(f'old trend: "{toptrend}". Sleep for {self.sleepTimeSkip} seconds')
                    time.sleep(self.sleepTimeSkip)
            intervallDuration = round(time.time() - intervalStartTime, 2) 
            logging.info(f"time to finish interval: {intervallDuration}. Range = {self.trendRange}")


def getArgs():
    parser = argparse.ArgumentParser(description='StatistikBot Hilfe:')
    parser.add_argument('-u', dest='noStatusUpdate', action='store_true',
                        help="for debugging, don't update status")
    parser.add_argument('-d', dest='debugMode', action='store_true',
                        help='debug logging')
    parser.add_argument('-c', dest='consoleLogging', action='store_true',
                        help='Log to console, not to log file')
    parser.add_argument('-cacheonly', dest='onlyUseCache', action='store_true',
                        help='Only use the cache file for follows lookup')
    parser.add_argument('-location', dest='woeid', default=23424829, type=int,
                    help='location id for the current trends')
    parser.add_argument('-follows', dest='maxFollowLookups', default=720, type=int,
                    help='maximum number of follows lookups')
    parser.add_argument('-followsuser', dest='maxFollowLookupsPerUser', default=2, type=int,
                    help='maximum number of follows lookups per user')
    parser.add_argument('-searches', dest='maxSearchResults', default=10000, type=int,
                    help='maximum number of search tweets')
    parser.add_argument('-trange', dest='trendRange', default=8, type=int,
                    help='number of top trends')
    parser.add_argument('-rrange', dest='rankingRange', default=10, type=int,
                    help='number of ranked users')
    parser.add_argument('-interval', dest='trendIntervallTime', default=14, type=int,
                    help='Interval time in hours')
    parser.add_argument('-trend', dest="customTrend", type=str,
                    help="generates statistics about custom trend. Excpects string argument.")
    parser.add_argument('-likes', dest="tweetLikes", type=int,
                    help="generates statistics about single tweet likes. Excpects string argument.")
    parser.add_argument('-replyto', dest="replyto", type=int, default=None,
                    help="ID, reply to status")
    parser.add_argument('-sleep', dest='sleepTimeSkip', default=60, type=int,
                    help='Time to sleep after old or invalid trend.')
    parser.add_argument('-utoken', dest='updateToken', default=0, type=int,
                    help='Token index used for status update')
    return parser.parse_args()

if __name__ == "__main__":
    args = getArgs()
    bot = statistikBot()

    bot.noStatusUpdate = args.noStatusUpdate
    bot.debugMode = args.debugMode
    bot.consoleLogging = args.consoleLogging
    bot.woeid = args.woeid
    bot.maxFollowLookups = args.maxFollowLookups
    bot.maxFollowLookupsPerUser = args.maxFollowLookupsPerUser
    bot.maxSearchResults = args.maxSearchResults
    bot.trendRange = args.trendRange
    bot.rankingRange = args.rankingRange
    bot.trendIntervallTime = args.trendIntervallTime
    bot.onlyUseCache = args.onlyUseCache
    bot.sleepTimeSkip = args.sleepTimeSkip
    bot.updateToken = args.updateToken

    bot.setupLogging()
    bot.readCredentials(os.path.join(os.path.dirname(__file__), 'token.json'))
    bot.setUpAPI()
    bot.loadCache()
    if args.customTrend:
        trend = args.customTrend.replace("'", "").replace('"','')
        bot.tweetTrendStats(trend, reply_to=args.replyto)
    elif args.tweetLikes:
        tweet = args.tweetLikes
        bot.tweetLikeStats(tweet, reply_to=args.replyto)
    else:
        bot.trendLoop()


